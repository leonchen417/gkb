import sqlite3
import time
import flask
import ipdb
import redis
from rq import Queue
from model_operations import (
    reverse_wip_status_if_not_deleted_background,
    get_current_job_queue,
    insert_job_data_to_job_queue,
    delete_from_job_queue,
    insert_wip_data_to_sql,
    delete_job_from_wip,
)

app = flask.Flask(__name__)
app.config["DEBUG"] = True
# DB
sql_file = "jobs.sql"

# init job queue
r = redis.Redis()
q = Queue(connection=r)


@app.route("/", methods=["GET"])
def home():
    return "<h1>Home Landing</h1><p>Landing page for home</p>"


@app.route("/jobs", methods=["POST"])
def post_job():
    new_job = flask.request.get_json()
    insert_job_data_to_job_queue(new_job)
    job_queue = get_current_job_queue()
    return f"payload: {new_job}, curr job: {job_queue}"


@app.route("/jobs/next", methods=["GET", "PATCH"])
def get_next_job():
    if flask.request.method == "GET":
        job_queue = get_current_job_queue()
        return f"{job_queue[0]}"
    elif flask.request.method == "PATCH":
        # move the working into a temporary db
        req = flask.request.get_json()  # for {"status": "Processing"}
        job_queue = get_current_job_queue()
        working_in_progress = job_queue.pop(0)
        delete_from_job_queue(working_in_progress["job_id"])
        insert_wip_data_to_sql(working_in_progress)
        redis_job = q.enqueue(
            reverse_wip_status_if_not_deleted_background, working_in_progress
        )
        return f"WIP: {working_in_progress}. NEW job_queue: {job_queue}, redis_job timestamp: {redis_job.enqueued_at}"


@app.route("/jobs/<wip_id>", methods=["DELETE"])
def delete_wip(wip_id):
    row_affected = delete_job_from_wip(wip_id)
    return f"DELETEed WIP job_id: {wip_id}, affecting {row_affected} rows"


if __name__ == "__main__":
    app.run()
