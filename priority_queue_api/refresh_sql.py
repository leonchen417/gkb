import sqlite3
import ipdb


def delete_all_data():
    conn = sqlite3.connect("jobs.sql")

    delete_all_data_from_wip_jobs = """ DELETE FROM wip_jobs;"""
    c = conn.cursor()
    c.execute(delete_all_data_from_wip_jobs)

    delete_all_data_from_job_queue = "DELETE FROM job_queue;"

    c.execute(delete_all_data_from_job_queue)
    conn.commit()
    conn.close()


if __name__ == "__main__":
    delete_all_data()
