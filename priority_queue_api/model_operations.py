import sqlite3
import time

sql_file = "jobs.sql"


def get_current_job_queue():
    conn = sqlite3.connect(sql_file)
    c = conn.cursor()
    select_sql = "SELECT * FROM job_queue;"
    c.execute(select_sql)
    job_queue_list = c.fetchall()
    job_queue = [
        {
            "job_id": job[0],
            "submitterId": job[1],
            "priority": int(job[2]),
            "name": job[3],
        }
        for job in job_queue_list
    ]
    return sorted(job_queue, key=lambda x: x["priority"])


def insert_job_data_to_job_queue(job):
    if "job_id" in job:
        enqueue_job = """ INSERT INTO job_queue (jobId, submitterId, priority, name) VALUES (?,?,?,?) """
        to_insert = (job["job_id"], job["submitterId"], job["priority"], job["name"])
    else:
        enqueue_job = (
            """ INSERT INTO job_queue (submitterId, priority, name) VALUES (?,?,?) """
        )
        to_insert = (job["submitterId"], job["priority"], job["name"])
    conn = sqlite3.connect(sql_file)
    cur = conn.cursor()
    cur.execute(enqueue_job, to_insert)
    conn.commit()
    conn.close()


def delete_from_job_queue(job_id):
    delete_sql = f"DELETE FROM job_queue WHERE jobId={job_id}"
    conn = sqlite3.connect(sql_file)
    cur = conn.cursor()
    cur.execute(delete_sql)
    conn.commit()
    conn.close()


def get_job_from_wip(job_id):
    sql = f"""SELECT * FROM wip_jobs WHERE jobId={job_id}"""
    conn = sqlite3.connect(sql_file)
    cur = conn.cursor()
    cur.execute(sql)
    to_reverse_wip = cur.fetchall()
    if to_reverse_wip:
        to_reverse_wip = to_reverse_wip[0]
    conn.close()
    return to_reverse_wip


def insert_wip_data_to_sql(wip):
    insert_wip_job_sql = """ INSERT INTO wip_jobs (jobId, submitterId, priority, name) VALUES (?,?,?,?) """
    to_insert = (wip["job_id"], wip["submitterId"], wip["priority"], wip["name"])
    conn = sqlite3.connect(sql_file)
    cur = conn.cursor()
    cur.execute(insert_wip_job_sql, to_insert)
    conn.commit()
    conn.close()


def delete_job_from_wip(job_id):
    conn = sqlite3.connect(sql_file)
    cur = conn.cursor()
    delete_sql = f"DELETE FROM wip_jobs WHERE jobId={job_id}"
    res = cur.execute(delete_sql)
    print(f"Delete job {job_id} from wip_jobs, affecting : {res.rowcount} rows")
    conn.commit()
    conn.close()
    return res.rowcount


def reverse_wip_status_if_not_deleted_background(wip):
    print(f"reverse_wip_status_if_not_deleted_background called with wip: {wip}")
    time.sleep(30)
    job_id = wip["job_id"]
    job_queue = get_current_job_queue()
    to_reverse_wip = get_job_from_wip(job_id)
    if not to_reverse_wip:
        print(f"{wip} has been deleted - no reverse, current job_queue: {job_queue}")
        return
    to_reverse_wip = {
        "job_id": int(to_reverse_wip[0]),
        "submitterId": int(to_reverse_wip[1]),
        "priority": int(to_reverse_wip[2]),
        "name": to_reverse_wip[3],
    }
    insert_job_data_to_job_queue(to_reverse_wip)
    job_queue = get_current_job_queue()
    print(f"reversed the wip object: {to_reverse_wip}, new job_queue: {job_queue}")
    _ = delete_job_from_wip(job_id)
    return "finished"
