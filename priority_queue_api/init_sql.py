import sqlite3
import ipdb


def create_job_queue_and_wip():
    conn = sqlite3.connect("jobs.sql")

    create_wip_jobs_sql = """ CREATE TABLE IF NOT EXISTS wip_jobs (jobId integer PRIMARY KEY, submitterId integer NOT NULL, priority integer NOT NULL, name text NOT NULL);"""
    c = conn.cursor()
    c.execute(create_wip_jobs_sql)

    create_job_queue_sql = "CREATE TABLE IF NOT EXISTS job_queue (jobId integer PRIMARY KEY, submitterId integer NOT NULL, priority integer NOT NULL, name text NOT NULL);"

    c.execute(create_job_queue_sql)
    conn.commit()
    conn.close()


def insert_init_dummy_job_data():
    job_queue = [
        {"submitterId": 1, "priority": 16, "name": "eat some snacks"},
        {"submitterId": 1, "priority": 1, "name": "take a bath"},
        {"submitterId": 2, "priority": 4, "name": "run a mile"},
        {"submitterId": 3, "priority": 8, "name": "workout 30 min"},
    ]

    conn = sqlite3.connect("jobs.sql")
    c = conn.cursor()
    for job in job_queue:
        submitterId = job["submitterId"]
        priority = job["priority"]
        name = job["name"]
        insert_job_sql = f"INSERT INTO job_queue (submitterId, priority, name) VALUES ({submitterId}, {priority}, '{name}');"
        c.execute(insert_job_sql)
        conn.commit()
    conn.close()


if __name__ == "__main__":
    create_job_queue_and_wip()
    insert_init_dummy_job_data()
